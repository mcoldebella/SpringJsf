package com.neoris.springapp.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.neoris.springapp.service.PersonaService;

@Controller
@RequestMapping("/")
public class AppController {

    protected final Log logger = LogFactory.getLog(getClass());
    
    @Autowired
	private PersonaService servicePersona;
    
    @RequestMapping(value="/home.htm")
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Returning hello view");
        servicePersona.saludar();
        return new ModelAndView("home");
    }
   
//    @RequestMapping(value="/list")
//    public ModelAndView handleRequest2(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        logger.info("Returning hello view");
//
//        return new ModelAndView("WEB-INF/views/listaEmpleados.xhtml");
//    }
    
//    @RequestMapping(value = { "/list.htm" }, method = RequestMethod.GET)
//	public String index(ModelMap model) {		
//    	return new ModelAndView("home");
//	}
    
    @RequestMapping(value="/list.htm")
    public ModelAndView handleRequest2(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Returning hello view");

        return new ModelAndView("listaEmpleados");
    }
}