package com.neoris.springapp.dao;

import com.neoris.springapp.modelo.Persona;

public interface PersonaDao {
	
	void savePersona(Persona persona);
	
}
