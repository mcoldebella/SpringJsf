package com.neoris.springapp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.neoris.springapp.modelo.DbConnection;
import com.neoris.springapp.modelo.Persona;
import com.neoris.springapp.service.PersonaService;
import com.neoris.springapp.service.PersonaServiceImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;



@ManagedBean(name="personaBean")
@RequestScoped
//@Controller
public class personaBean {
	
	private Persona persona = new Persona();
	private List<Persona> lstPersonas = new ArrayList<Persona>();
	
//	@Autowired
//	PersonaService servicePersona;
	
	public personaBean(){
		
	}
	
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public List<Persona> getLstPersonas() {
		return lstPersonas;
	}
	public void setLstPersonas(List<Persona> lstPersonas) {
		this.lstPersonas = lstPersonas;
	}
	
	public void registrarPersona(){
		System.out.println("Entrando al metodo q llama al servicio , nombre de la persona = "+this.persona.getNombre());
//		personaBean.lstPersonas.add(this.persona);
//		servicePersona.savePersona(this.persona);

		 
		  
		System.out.println("nombre obtenido = "+this.persona.getNombre());
	}
	
	public void eliminarPersona(Persona persona){
		this.lstPersonas.remove(persona);
	}
	
	
	
}
