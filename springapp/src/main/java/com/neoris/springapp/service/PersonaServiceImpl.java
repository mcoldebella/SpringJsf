package com.neoris.springapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neoris.springapp.dao.PersonaDao;
import com.neoris.springapp.dao.PersonaDaoImpl;
import com.neoris.springapp.modelo.Persona;

@Service("personaService")
@Transactional
public class PersonaServiceImpl implements PersonaService {
	
	@Autowired
	private PersonaDao dao;
	
	public void savePersona(Persona persona) {	
		System.out.println("entra al servicio");
		dao.savePersona(persona);
	}

	public void saludar() {
		System.out.println("Holaaaaa");
		
	}
	
}
