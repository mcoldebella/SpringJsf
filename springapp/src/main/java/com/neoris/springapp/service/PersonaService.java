package com.neoris.springapp.service;

import com.neoris.springapp.modelo.Persona;

public interface PersonaService {
	
	public void savePersona(Persona persona);
	public void saludar();
}
